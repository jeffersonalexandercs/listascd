/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.pruebas;

import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author Navas-JC
 */
public class prueba1  //contiene elementos repetidos
{
    public static void main(String[] args) {
        ListaCD<Integer> lista = new ListaCD<>();
        try {
            lista.insertarAlFinal(10);
            lista.insertarAlFinal(2);
            lista.insertarAlFinal(3);
            lista.insertarAlFinal(32);
            lista.insertarAlFinal(23);
            lista.insertarAlFinal(23);
            System.out.println((lista.contieneElemRepetidos()) ? "si contiene" : "no contiene");
//            
//            
        } catch (ExceptionUFPS ex) {
            System.err.println(ex.getMensaje());
        }
    }
    
    
    }
    

